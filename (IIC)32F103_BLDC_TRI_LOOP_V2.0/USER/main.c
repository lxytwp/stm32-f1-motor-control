/*  
############  author Fu Z  ############ 
Usart3 为上位机串口
TIMER1中运行状态机
*/
#include "My_Define.h"

int main(void)
{
  // ************* system init ************  
	SystemInit();
	delay_init();	    	 //延时函数初始化	  
	// ************* device init ************
	
	// ##### init structs #####
	// 1秒10000次计算，一次中断则有Ts = 1/Fs秒。假设额定频率为fn，一个周期用时1/fn秒。将额定频率时步长标幺化：fn/Fs
	ctl_struc_init();
 	LED_INIT();			     //LED端口初始化 
	EXTI_KEY_Config();
	Adc_Init();		  		//ADC初始化	   
	TIM3_Init(300,72);  //  10ms 
	IIC_Init();
	// ************* communication init  **************
		//USART2_Init(921600);	
		uart2_init(921600);
		// ************* main interrupt init *************
	pwm_init();
	
	while(1)
	{
		usart2_dma_run();
		
	}
}

