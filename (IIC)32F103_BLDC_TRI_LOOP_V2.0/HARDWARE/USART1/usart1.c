/*  
############  author Fu Z  ############ 
串口To 上位机
*/
#include "sys.h"		    
#include "usart1.h"
#include "usart2.h"
#include "usart3.h"
#include "delay.h"
#include "led.h"
#include "nvic.h"
#include "timer.h"
#include "stm32f10x_tim.h"
#include "stdio.h"
#include "My_Define.h"
//////////////////////////////////////////////////////////////////////////////////
// *********  串口1-PC  **********

#define head            0x55
#define device_address  0xaa
#define point_address   0x14

#define USARTx                   USART1
#define PeriphClock_USARTx       RCC_APB2Periph_USART1
#define PeriphClock_USARTx_GPIO  RCC_APB2Periph_GPIOA
#define USARTx_Pin_TX            GPIO_Pin_9  
#define USARTx_Pin_RX            GPIO_Pin_10  
#define USARTx_Pin_GPIO_Group    GPIOA

////////////////////////////////////////////////////////////////////////////////// 	 
  	  
//接收缓存区 	
u8 USART1_RX_BUF[640];  	//接收缓冲,最大64个字节.
//接收到的数据长度
u8 USART1_RX_CNT;   


void USART1_GPIO_Init(void)
{
	  GPIO_InitTypeDef GPIO_InitStructure;
	////////////////////////////////////////////////////////////////////////////////// 			
	 //USARTx_TX  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9; //
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	//复用推挽输出
  GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.2
   
  //USARTx_RX	  
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;//
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;//浮空输入
  GPIO_Init(GPIOA, &GPIO_InitStructure);//初始化GPIOA.3 
   
	//GPIO_ResetBits(GPIOA,GPIO_Pin_9|GPIO_Pin_10);		
////////////////////////////////////////////////////////////////////////////////// 

}

void USART1_Init(u32 bound)        //初始化IO 串口2
{  	 
	
 USART_InitTypeDef USART_InitStructure;
	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO,ENABLE); //  串口xGPIO口时钟使能
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);// 串口x时钟使能
	
  ////////////////////////////////////////////////////////////////////////////////// 			 
   //USART 初始化设置
	USART_InitStructure.USART_BaudRate = bound;//串口波特率
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;//字长为8位数据格式
	USART_InitStructure.USART_StopBits = USART_StopBits_1;//一个停止位
	USART_InitStructure.USART_Parity = USART_Parity_No;//无奇偶校验位
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;//无硬件数据流控制
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;	//收发模式
  USART_Init(USART1, &USART_InitStructure); //初始化串口1
	
  USART_Cmd(USART1, ENABLE);                    //使能串口1
  USART_ClearFlag(USART1, USART_FLAG_TC);


#if EN_USART1_RX	

	USART_ITConfig(USART1, USART_IT_IDLE, ENABLE);//
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);//开启接受中断

NVIC_Config(3);


#endif	
	
	USART1_GPIO_Init();
}

//**************串口1发送一位数据*************
void USART1_Send_Byte(unsigned char byte)       
{
   USART_SendData(USART1, byte);        
   while( USART_GetFlagStatus(USARTx,USART_FLAG_TC)!= SET);  
     
}

//**************串口1中断服务函数*************
void USART1_IRQHandler(void)                   //
{
	u8 state = USART1->SR;
	u8 res = USART1->DR;
	
	//字符中断
	if(state& USART_FLAG_RXNE)				//接收到数据
	{	 		
		USART_ClearITPendingBit(USART1, USART_IT_RXNE);
	  //res =USART_ReceiveData(USART1);														//读取接收到的数据USARTx->DR
		USART1_RX_BUF[USART1_RX_CNT++]=res;		
	} 
	
	//空闲中断
  else if(state& USART_FLAG_IDLE)	 //接收完毕进入数据校验、处理、解析
	{ 
		USART_ClearITPendingBit(USART1, USART_IT_IDLE);

		USART1_info_deal();	
		USART1_RX_CNT=0;	
	} 

} 

	
void USART1_info_deal(void)
{

}

//***** 接收AUV的位置信息 *******

void USART1_data_translate()   
{

}



